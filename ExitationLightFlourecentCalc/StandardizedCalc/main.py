import cv2
import numpy as np
from numpy.core.numeric import NaN
from numpy.lib.type_check import imag
from skimage import measure
from imutils import contours
import imutils
import pandas as pd
import os
import matplotlib.pyplot as plt




def luxCalc(file,name,x,y,h,w,well):
    img = cv2.imread(file)
    
    #convert to grayscale
    
    pts1 = np.float32(
        [[405, 1], # top left
         [2257, 1], # top right
         [393, 1769], # bottom left
         [2257, 1785]] # bottom right
        )

    pts2 = np.float32(
        [[0,0], # top left
         [1865,0], # top right
         [0,1809], # bottom left
         [1865,1809]] # bottom right
         )
    matrix = cv2.getPerspectiveTransform(pts1,pts2)
    image = cv2.warpPerspective(img, matrix, (1865,1809)) # set size image based on pts2 (w = 1850 h = 1790)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # image blurring
    blurred = cv2.GaussianBlur(gray, (11, 11), 0)

    crop = blurred[y:y+h,x:x+w]

    
   
    
    brightness[f'Well #{well}'] =  np.average(crop)
    brightnessimg = str(round(brightness[f'Well #{well}'],2))
    




    cv2.circle(image, (int(x+42), int(y+64)), int((w/2)+10),(0, 0, 255), 3)
    cv2.putText(image, brightnessimg, (x, y - 15),
	cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)




    

    

"""
    plt.subplot(1,2,1),
    plt.imshow(cv2.cvtColor(img,cv2.COLOR_BGR2RGB))
    plt.title('Image')

    plt.subplot(1,2,2),
    plt.imshow(cv2.cvtColor(image,cv2.COLOR_BGR2RGB))
    plt.title('Result')

    plt.show()
    cv2.waitKey(0)

    plt.subplot(1,2,1),
    plt.imshow(cv2.cvtColor(image,cv2.COLOR_BGR2RGB))
    plt.title('Original Image')

    plt.subplot(1,2,2),
    plt.imshow(cv2.cvtColor(blurred,cv2.COLOR_BGR2RGB))
    plt.title('Result')

    plt.show()
    cv2.waitKey(0)

    plt.subplot(1,2,1),
    plt.imshow(cv2.cvtColor(crop,cv2.COLOR_BGR2RGB))
    plt.title('Original Image')

    plt.subplot(1,2,2),
    plt.imshow(cv2.cvtColor(blurred,cv2.COLOR_BGR2RGB))
    plt.title('Result')

    plt.show()
    cv2.waitKey(0)

"""
def analysis():
    brightnessDf.set_index('Angle', inplace=True)
    brightnessDf.to_csv('trial-7.csv')  
    x = brightnessDf.mean(axis = 0)

    #Histogram
    ax= x.plot.hist()
    fig = ax.get_figure()
    fig.savefig('C:/Users/varun/Desktop/plateReader/ExitationLightFlourecentCalc/savePlot/otherfigure.pdf')

    #scatter
    
    df_reset=brightnessDf.reset_index()
    """
    scatter = df_reset.plot.scatter(x = "Angle",y = "Well #1", title= "Scatter plot")
    fig = scatter.get_figure()
    fig.savefig('C:/Users/varun/Desktop/plateReader/ExitationLightFlourecentCalc/savePlot/otherscatter.pdf')
    df_reset=brightnessDf.reset_index()
    
    """
    
    #line
    line = df_reset.plot(x="Angle", y="Well #1")
    fig = line.get_figure()
    fig.savefig('C:/Users/varun/Desktop/plateReader/ExitationLightFlourecentCalc/savePlot/otherlineGraph.pdf')



    #PLOTTTTIINGGGGGG BREUHHHHHh


    


if __name__ == '__main__':
    angle = []
    positions = {1:[781,477,81,111], 2:[985,473,87,107], 3:[791,277,68,94], 4: [985,265,81,109], 5:[757,1075,109,131],  6:[969,1067,119,117]}

    brightnessDf = pd.DataFrame()
    directory = r'C:\Users\varun\Desktop\plateReader\ExitationLightFlourecentCalc\trial-7'
    file = ""
    for cnt,filename in enumerate(os.listdir(directory)):
        bright=[]
        brightness = {}
        if filename.endswith(".jpg") or filename.endswith(".png") or filename.endswith(".jpeg"):
            x = os.path.join(directory, filename)
            x = x.replace('\\', "/")
            for i in range(1,7):
                luxCalc(x, file,positions[i][0],positions[i][1],positions[i][2],positions[i][3],i)    #replace values with my value huwewehuwehu
            #append dict values into df
            brightnessDf = brightnessDf.append(brightness, ignore_index=True)
            angle.append(filename.replace(".png", ""))  
    brightnessDf.insert(0, "Angle",angle, True)
    
    analysis() #what are you supposed to analyze
 














