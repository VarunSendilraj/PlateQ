import cv2 
import numpy as np 
import matplotlib.pyplot as plt
import pandas as pd



brightnessDf = pd.DataFrame()
brightness = {}


# Read image. 
img = cv2.imread('C:/Users/varun/Desktop/plateReader/ExitationLightFlourecentCalc/testImg/breuh.png', cv2.IMREAD_COLOR) 

# Convert to grayscale. 
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) 

# Blur using 3 * 3 kernel. 
gray_blurred = cv2.blur(gray, (3, 3)) 


# Apply Hough transform on the blurred image. 
detected_circles = cv2.HoughCircles(gray_blurred, 
				cv2.HOUGH_GRADIENT, 1, 20, param1 = 50, 
			param2 = 30, minRadius = 24, maxRadius = 40) 



# Draw circles that are detected. 
if detected_circles is not None: 

	# Convert the circle parameters a, b and r to integers. 
	detected_circles = np.uint16(np.around(detected_circles)) 

	for cnt, pt in enumerate(detected_circles[0, :]): 
		x, y, r = int(pt[0]), int(pt[1]), int(pt[2])
		crop_img = img[x:x+r, y:y+r]
		cv2.imwrite(f"C:/Users/varun/Desktop/plateReader/ExitationLightFlourecentCalc/savePlot/img{cnt}.jpg",crop_img)
		brightness[f'Well #{cnt+1}'] = np.average(crop_img)
		brightnessimg = str(round(brightness[f'Well #{cnt+1}'],2))
		
	



		# Draw the circumference of the circle. 
		cv2.circle(img, (x, y), r, (0, 255, 0), 2) 
        #text = "Well " + 

		# Draw a small circle (of radius 1) to show the center. 
		cv2.circle(img, (x, y), 1, (0, 0, 255), 3) 
        #cv2.putText(img, text, (a, b - 15),cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
		
cv2.imshow("Detected Circle", img) 
