import cv2
import numpy as np
import matplotlib.pyplot as plt

img = cv2.imread("C:/Users/varun/Desktop/plateReader/DiffusorTest/image.jpg")



pts1 = np.float32(
        [[873, 1001], # top left
         [2805, 1001], # top right
         [884, 2319], # bottom left
         [2861, 2273]] # bottom right
)

pts2 = np.float32(
        [[0,0], # top left
         [1957,0], # top right
         [0,2273], # bottom left
         [1957,1267]] # bottom right
         )

matrix = cv2.getPerspectiveTransform(pts1,pts2)
result = cv2.warpPerspective(img, matrix, (1957,1267)) # set size image based on pts2 (w = 500 h = 600)


cv2.imwrite("C:/Users/varun/Desktop/plateReader/ExitationLightFlourecentCalc/savePlot/1.jpg",result)
plt.subplot(1,2,1),
plt.imshow(cv2.cvtColor(img,cv2.COLOR_BGR2RGB))
plt.title('Original Image')

plt.subplot(1,2,2),
plt.imshow(cv2.cvtColor(result,cv2.COLOR_BGR2RGB))
plt.title('Result')

plt.show()
cv2.waitKey(0)