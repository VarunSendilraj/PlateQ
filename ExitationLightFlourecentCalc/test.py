import cv2
import numpy as np
from numpy.lib.type_check import imag
from numpy.linalg import norm
import argparse
from skimage import measure
from imutils import contours
import imutils
import skimage
import pandas as pd

brightness = {}


df = pd.DataFrame()


# Image 


#get the image 
image = cv2.imread('C:/Users/varun/Desktop/plateReader/ExitationLightFlourecentCalc/testImg/wellCHECK.jpg')
#convert to grayscale
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# image blurring
blurred = cv2.GaussianBlur(gray, (11, 11), 0)

# apply threshhold for brightest spots (essentially to find the brighest points in the image)
thresh = cv2.threshold(blurred, 130, 255, cv2.THRESH_BINARY)[1]

# clean up the threshold image
thresh =  cv2.erode(thresh, None, iterations=9)
thresh = cv2.dilate(thresh, None, iterations=8)



labels = measure.label(thresh, background=0)
mask = np.zeros(thresh.shape, dtype="uint8")
# loop over the unique components
for label in np.unique(labels):
	# if this is the background label, ignore it
	if label == 0:
		continue
	# otherwise, construct the label mask and count the
	# number of pixels 
	labelMask = np.zeros(thresh.shape, dtype="uint8")
	labelMask[labels == label] = 255
	numPixels = cv2.countNonZero(labelMask)
	# if the number of pixels in the component is sufficiently
	# large, then add it to our mask of "large blobs"
	if numPixels > 300:
		mask = cv2.add(mask, labelMask)
#cv2.imwrite("saved_new.jpg", blurred)
#(minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(thresh)
#image = image.copy()
#cv2.circle(image, maxLoc, 50, (255, 0, 0), 2)
# display the results of our newly improved method
#cv2.imshow("Robust", image)
#cv2.waitKey(0)

cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
cnts = contours.sort_contours(cnts)[0]



# loop over the contours
for (i, c) in enumerate(cnts):
	# draw the bright spot on the image
	(x, y, w, h) = cv2.boundingRect(c)
	((cX, cY), radius) = cv2.minEnclosingCircle(c)
	crop = blurred[y:y+h,x:x+w]
	# hsvIMAGE = cv2.cvtColor(crop, cv2.COLOR_BGR2HSV ) 
	# print(hsvIMAGE)
	cv2.imshow("cropped", crop)
	cv2.waitKey(0)
	

	brightness[f'Well #{i+1}'] = np.average(crop)
	brightnessimg = str(round(brightness[f'Well #{i+1}'],2))

	cv2.circle(image, (int(cX), int(cY)), int(radius),
		(0, 0, 255), 3)
	cv2.putText(image, brightnessimg, (x, y - 15),
		cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
	
#show the output image
cv2.imshow("Labeled", image)
cv2.waitKey(0)
cv2.imshow("GreyBlurred", blurred)
cv2.waitKey(0)
cv2.imshow("Thresh", thresh)
cv2.waitKey(0)
