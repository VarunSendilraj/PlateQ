import numpy as np
from PIL import Image
import glob





imgList = glob.glob('*.jpg')
print(imgList)
first = True
for img in imgList:
    temp = np.asarray(Image.open(img))
    temp = temp.astype('uint32')
    if first:  
        sumImage = temp
        first = False
    else:
        sumImage = sumImage + temp
        
    avgArray = sumImage/len(imgList)
    avgImg = Image.fromarray(avgArray.astype('uint8'))
    avgImg.save("image.jpg")
    print("yes")
