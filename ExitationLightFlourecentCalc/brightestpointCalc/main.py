import cv2
import numpy as np
from skimage import measure
from imutils import contours
import imutils
import pandas as pd
import os
import matplotlib.pyplot as plt




def luxCalc(file,name):
    image = cv2.imread(file)
    #convert to grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # image blurring
    blurred = cv2.GaussianBlur(gray, (11, 11), 0)

    # apply threshhold for brightest spots (essentially to find the brighest points in the image)
    thresh = cv2.threshold(blurred, 125, 255, cv2.THRESH_BINARY)[1]

    # clean up the threshold image
    thresh =  cv2.erode(thresh, None, iterations=9)
    thresh = cv2.dilate(thresh, None, iterations=8)

    labels = measure.label(thresh, background=0)
    mask = np.zeros(thresh.shape, dtype="uint8")
    # loop over the unique components
    for label in np.unique(labels):
	    # ignore it backround label
	    if label == 0:
		    continue
	    # otherwise, construct the label mask and count the
	    # number of pixels 
	    labelMask = np.zeros(thresh.shape, dtype="uint8")
	    labelMask[labels == label] = 255
	    numPixels = cv2.countNonZero(labelMask)

	    # if brightness large enough add to mask
	    if numPixels > 300:
		    mask = cv2.add(mask, labelMask)

    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    cnts = contours.sort_contours(cnts)[0]

    # loop over the contours
    for (i, c) in enumerate(cnts):
	    # draw the bright spot on the image
	    (x, y, w, h) = cv2.boundingRect(c)
	    ((cX, cY), radius) = cv2.minEnclosingCircle(c)
	    crop = blurred[y:y+h,x:x+w]


	    brightness[f'Well #{i+1}'] = np.average(crop)
	    brightnessimg = str(round(brightness[f'Well #{i+1}'],2))


	    cv2.circle(image, (int(cX), int(cY)), int(radius),(0, 0, 255), 3)
	    cv2.putText(image, brightnessimg, (x, y - 15),
		cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
    angle.append(name)

    
    #show the output image 
    cv2.imshow("Image", image)
    cv2.waitKey(0)
    cv2.imshow("Image", blurred)
    cv2.waitKey(0)


def analysis():
    brightnessDf.set_index('Angle', inplace=True)
    brightnessDf.to_csv('test.csv') 
    x = brightnessDf.mean(axis = 0)

    #Histogram
    ax= x.plot.hist()
    fig = ax.get_figure()
    fig.savefig('C:/Users/varun/Desktop/plateReader/ExitationLightFlourecentCalc/savePlot/figure.pdf')
    

    

    #scatter
    df_reset=brightnessDf.reset_index()
    scatter = df_reset.plot.scatter(x = "Angle",y = "Well #1",title= "Scatter plot")
    fig = scatter.get_figure()
    fig.savefig('C:/Users/varun/Desktop/plateReader/ExitationLightFlourecentCalc/savePlot/scatter.pdf')
    df_reset=brightnessDf.reset_index()

    #line
    line = df_reset.plot(x="Angle", y=["Well #1", "Well #2", "Well #3"])
    fig = line.get_figure()
    fig.savefig('C:/Users/varun/Desktop/plateReader/ExitationLightFlourecentCalc/savePlot/lineGraph.pdf')

    



    #PLOTTTTIINGGGGGG BREUHHHHHh

    

    

if __name__ == '__main__':
    angle = []
    brightnessDf = pd.DataFrame()
    directory = r'C:\Users\varun\Desktop\plateReader\ExitationLightFlourecentCalc\testImg'
    file = ""
    for filename in os.listdir(directory):
        brightness = {}
        if filename.endswith(".jpg") or filename.endswith(".png") or filename.endswith(".jpeg"):
            x = os.path.join(directory, filename)
            x = x.replace('\\', "/")
            file = filename.replace(".jpg", "")
            luxCalc(x, file)
            #append dict values into df
            brightnessDf = brightnessDf.append(brightness, ignore_index=True)
    brightnessDf.insert(0, "Angle",angle, True)
    

    analysis() #what are you supposed to analyze












